# README

This is a simple tool intended for generating yEd UML digrams from Java code.

Eventually, it should be able to reverse the process and generate java code stubs from valid diagrams.

## Build instructions

Build with:

    mvn clean install