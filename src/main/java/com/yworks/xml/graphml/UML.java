//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.09.16 at 03:37:30 PM CEST 
//


package com.yworks.xml.graphml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.yworks.com/xml/graphml}AttributeLabel"/>
 *         &lt;element ref="{http://www.yworks.com/xml/graphml}MethodLabel"/>
 *       &lt;/sequence>
 *       &lt;attribute name="clipContent" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="constraint" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="omitDetails" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="stereotype" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="use3DEffect" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "attributeLabel",
    "methodLabel"
})
@XmlRootElement(name = "UML")
public class UML {

    @XmlElement(name = "AttributeLabel", required = true)
    protected String attributeLabel;
    @XmlElement(name = "MethodLabel", required = true)
    protected String methodLabel;
    @XmlAttribute(name = "clipContent", required = true)
    protected boolean clipContent;
    @XmlAttribute(name = "constraint", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String constraint;
    @XmlAttribute(name = "omitDetails", required = true)
    protected boolean omitDetails;
    @XmlAttribute(name = "stereotype", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String stereotype;
    @XmlAttribute(name = "use3DEffect", required = true)
    protected boolean use3DEffect;

    /**
     * Gets the value of the attributeLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeLabel() {
        return attributeLabel;
    }

    /**
     * Sets the value of the attributeLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeLabel(String value) {
        this.attributeLabel = value;
    }

    /**
     * Gets the value of the methodLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodLabel() {
        return methodLabel;
    }

    /**
     * Sets the value of the methodLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodLabel(String value) {
        this.methodLabel = value;
    }

    /**
     * Gets the value of the clipContent property.
     * 
     */
    public boolean isClipContent() {
        return clipContent;
    }

    /**
     * Sets the value of the clipContent property.
     * 
     */
    public void setClipContent(boolean value) {
        this.clipContent = value;
    }

    /**
     * Gets the value of the constraint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstraint() {
        return constraint;
    }

    /**
     * Sets the value of the constraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstraint(String value) {
        this.constraint = value;
    }

    /**
     * Gets the value of the omitDetails property.
     * 
     */
    public boolean isOmitDetails() {
        return omitDetails;
    }

    /**
     * Sets the value of the omitDetails property.
     * 
     */
    public void setOmitDetails(boolean value) {
        this.omitDetails = value;
    }

    /**
     * Gets the value of the stereotype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStereotype() {
        return stereotype;
    }

    /**
     * Sets the value of the stereotype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStereotype(String value) {
        this.stereotype = value;
    }

    /**
     * Gets the value of the use3DEffect property.
     * 
     */
    public boolean isUse3DEffect() {
        return use3DEffect;
    }

    /**
     * Sets the value of the use3DEffect property.
     * 
     */
    public void setUse3DEffect(boolean value) {
        this.use3DEffect = value;
    }

}
