package dk.darknight.uml.cmdprsr;

import static dk.darknight.uml.cmdprsr.ValidOptions.CLASSPATH_OPT;
import static dk.darknight.uml.cmdprsr.ValidOptions.HELP_OPT;
import static dk.darknight.uml.cmdprsr.ValidOptions.OUTPUTFILE_OPT;
import static dk.darknight.uml.cmdprsr.ValidOptions.PACKAGEFILTER_OPT;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

/**
 * Helper class that parses and extracts command line parameters.
 * 
 * @see ValidOptions
 */
@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class CommandlineParser {
	/** Singleton containing the valid options for the application. */
	private static final Options OPTIONS = new ValidOptions();
	
	/**
	 * Parse the command parameters using Apache Commons CLI.
	 * 
	 * @param arguments
	 *            the arguments passed to {@link #main(String[])}
	 * @return a value object containing the parsed parameters, or
	 *         <code>null</code> if there was an error.
	 * @throws ParseException
	 *             if the command line is unintelligble
	 * @throws FileNotFoundException
	 *             if one of the classPathEntries in the search path is not
	 *             found
	 * @throws IllegalArgumentException
	 *             if there are no class path, either as a parameter or a list
	 *             files
	 */
    public static Parameters getParameters(String[] arguments) throws ParseException, FileNotFoundException {
		// parse the command line arguments
        CommandLine line = new BasicParser().parse(OPTIONS, arguments);

        if (line.hasOption(HELP_OPT)) {
            return null;
        }

        OutputStream outputStream = System.out;
        
        if (line.hasOption(OUTPUTFILE_OPT)) {
            // Redirect output to file (default is System.out otherwise)
        	File ofParam = (File) line.getParsedOptionValue(OUTPUTFILE_OPT);
            Preconditions.checkArgument(!ofParam.exists()
                    || (ofParam.exists() && ofParam.isFile()),
                    "'outputFile' must be a filename");

            outputStream = new FileOutputStream(ofParam);
        }

        // Build an aggregated class path        
        Set<File> classPathEntries = new HashSet<File>();
        
        if (line.hasOption(CLASSPATH_OPT)) {        	
			String[] fileNames = line.getOptionValues(CLASSPATH_OPT);
			classPathEntries.addAll(fileNamesToFiles(fileNames));
        }
                
		classPathEntries.addAll(fileNamesToFiles(line.getArgs()));

        Preconditions.checkArgument(classPathEntries.size() > 0,
                "At least one source directory must be provided");
                
        Set<String> packageFilter = new HashSet<String>();
        // Check for pckage prefix filtering
        if (line.hasOption(PACKAGEFILTER_OPT)) {
        	String[] prefixFilter = line.getOptionValues(PACKAGEFILTER_OPT);
            packageFilter = Sets.newHashSet(prefixFilter);
        }
        
        // Build the response object
        return Parameters.of(outputStream, classPathEntries, true, packageFilter);
    }

	/**
	 * Transform an array of directory- and filenames to a {@link Set} of
	 * {@link File}s.
	 * 
	 * @param fileNames
	 *            a set of JAR files and class directories in any combination
	 * @return a {@link Set} of {@link File}s
	 */
    @VisibleForTesting
	static Set<File> fileNamesToFiles(String[] fileNames) {
		return Arrays.stream(fileNames)
				.map(fn -> new File(fn))
				.filter(f -> f.exists())
				.collect(Collectors.toSet());
	}
    
    /** Print help message to stdout. */
    public static void printHelp() {
        new HelpFormatter().printHelp("yed-uml-tools", OPTIONS, true);
    }
}