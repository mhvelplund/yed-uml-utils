/**
 * This package contains the command line parsing. The main class is 
 * {@link dk.darknight.uml.cmdprsr.CommandlineParser CommandlineParser}'s 
 * {@link dk.darknight.uml.cmdprsr.CommandlineParser#getParameters(String[]) 
 * getParameters(String[])}-method. It looks at the command line and the options
 * defined in {@link dk.darknight.uml.cmdprsr.ValidOptions ValidOptions} 
 * and translates them to a {@link dk.darknight.uml.cmdprsr.Parameters Parameters} 
 * model object.
 */
package dk.darknight.uml.cmdprsr;