package dk.darknight.uml.cmdprsr;

import java.io.File;
import java.io.OutputStream;
import java.util.Set;

import lombok.NonNull;
import lombok.Value;

/** Model object for parsed command-line parameters. */
@Value(staticConstructor="of")
public class Parameters {
	/** The target for the generated graphml output. */
    @NonNull private OutputStream outputStream;

	/**
	 * A list of files or directories to be scanned for code. Duplicates are
	 * discarded.
	 */
    @NonNull private Set<File> classPathEntries;

	// Unsupported
	private boolean ignoreMissingDependencies;

	/**
	 * A set of prefixes.
	 * <p>
	 * Classes that match the package prefixes will have full UML descriptions.
	 * The rest will only be stubbed, and only if the are lined to a fully
	 * mapped class.
	 * </p>
	 */
	@NonNull private Set<String> packageFilter;
}