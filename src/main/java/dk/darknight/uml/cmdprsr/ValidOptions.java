package dk.darknight.uml.cmdprsr;

import java.io.File;

import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

/**
 * Represents a list of valid options for the application.
 */
class ValidOptions extends Options {
	private static final long serialVersionUID = 1L;

	static final String OUTPUTFILE_OPT = "o";
	static final String IGNOREMISSINGDEPS_OPT = "i";
	static final String HELP_OPT = "?";
	static final String CLASSPATH_OPT = "cp";
	static final String PACKAGEFILTER_OPT = "p";

	@SuppressWarnings("static-access")
	public ValidOptions() {
		addOption(OptionBuilder
				.isRequired(false)
				.hasArgs(1)
				.withLongOpt("outputFile")
				.withType(File.class)
				.withDescription("if a file is specified, the output will be directed there instead stdout")
				.create(OUTPUTFILE_OPT));
		addOption(OptionBuilder
				.isRequired(false)
				.hasArgs()
				.withValueSeparator(File.pathSeparatorChar)
				.withLongOpt("classPath")
				.withType(File.class)
				.withDescription("a classpath (separated by '" + File.pathSeparator + "') to scan for classes")
				.create(CLASSPATH_OPT));
		addOption(OptionBuilder
				.isRequired(false)
				.withLongOpt("help")
				.withDescription("print a list of valid arguments")
				.create(HELP_OPT));
//		addOption(OptionBuilder.isRequired(false)
//				.withLongOpt("ignoreMissingDeps")
//				.withDescription("stub missing dependencies")
//				.create(IGNOREMISSINGDEPS_OPT));
		addOption(OptionBuilder
				.isRequired(false)
				.hasArgs()
				.withValueSeparator(File.pathSeparatorChar)
				.withLongOpt("packageFilter")
				.withDescription("a list of package name prefixes separated by '" + File.pathSeparator + "'. If present, only classes in these will be described in detail")
				.create(PACKAGEFILTER_OPT));
	}
}
