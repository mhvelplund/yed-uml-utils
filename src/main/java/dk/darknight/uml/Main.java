package dk.darknight.uml;

import static dk.darknight.uml.cmdprsr.CommandlineParser.getParameters;
import static dk.darknight.uml.cmdprsr.CommandlineParser.printHelp;

import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import lombok.extern.slf4j.Slf4j;

import org.graphdrawing.graphml.xmlns.Graphml;

import dk.darknight.uml.cmdprsr.Parameters;
import dk.darknight.uml.generator.graphml.GraphGenerator;
import dk.darknight.uml.generator.uml.UmlGenerator;
import dk.darknight.uml.model.UmlEntity;
import dk.darknight.util.ClassUtils;

/** Main entry point for {@code yed-uml-tools} command line. */
@Slf4j
public class Main {

    /** Entry point. */
    public static void main(String[] args) {
        Parameters params = null;

        // Parse the command line parameters
        try {
            params = getParameters(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println();
        }
        
        if (params == null) {
            printHelp();
            return;
        }

        // Load all class definitions in the class-path		    
        Set<Class<?>> classes = ClassUtils.findClasses(params.getClassPathEntries());
        
        if (log.isTraceEnabled()) {
            classes.stream()
                    .sorted((c1, c2) -> c1.getName().compareTo(c2.getName()))
                    .forEach(c -> {
                        log.trace("Found class '{}'", c.getName());
                    });
        }
        
        // Build a meta-model of the UML representation of the classes
		List<UmlEntity> entities = UmlGenerator.getUml(classes, params.getPackageFilter());
		
		// Build a GraphML document based on the meta-model
		Graphml diagram = GraphGenerator.generateUmlDiagram(entities);
		
		// Serialize the GraphML document to the chosen output channel
        try {
            JAXBContext.newInstance(Graphml.class)
                    .createMarshaller()
                    .marshal(diagram, params.getOutputStream());
        } catch (JAXBException e) {
            System.err.println("Unable to generate graphml file due to a marshalling exception: "
                    + e.getMessage());
            log.debug("Caught JAXBException", e);
        }
	}

}
