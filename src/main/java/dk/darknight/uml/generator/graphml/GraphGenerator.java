package dk.darknight.uml.generator.graphml;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.graphdrawing.graphml.xmlns.Edge;
import org.graphdrawing.graphml.xmlns.Graph;
import org.graphdrawing.graphml.xmlns.Graphml;
import org.graphdrawing.graphml.xmlns.Key;

import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlEdge;
import dk.darknight.uml.model.UmlEntity;

public class GraphGenerator {
    public static <T extends UmlEntity> Graphml generateUmlDiagram(
            List<T> entities) {
        GraphGenerator generator = new GraphGenerator();

        for (T t : entities) {
            if (t instanceof UmlClass) {
                generator.append((UmlClass) t);
            } else if (t instanceof UmlEdge) {
                generator.append((UmlEdge) t);
            } else {
                throw new IllegalArgumentException(MessageFormat.format(
                        "Unsupported UmlEntity: {0}", t.getClass().getName()));
            }
        }

        return generator.graphml;
    }
    
    static final String NODE_KEY_ID = "d6";
    static final String EDGE_KEY_ID = "d10";

    private final ClassHandler classHandler;
    private final EdgeHandler edgeHandler;    
    private final Graphml graphml;
    private final Map<Integer, String> nodes;

    private GraphGenerator() {
        Graph graph = new Graph();
        graph.setEdgedefault("directed");
        graph.setId("G");
        
        Key nodeKey = new Key();
        nodeKey.setFor("node");
        nodeKey.setId(NODE_KEY_ID);
        nodeKey.setYfilesType("nodegraphics");
        
        Key edgeKey = new Key();
        edgeKey.setFor("edge");
        edgeKey.setId(EDGE_KEY_ID);
        edgeKey.setYfilesType("edgegraphics");
        
        graphml = new Graphml();
        graphml.getKey().add(nodeKey);
        graphml.getKey().add(edgeKey);
        graphml.setGraph(graph);
        
        nodes = new HashMap<Integer, String>();
        
        classHandler = ClassHandler.of(graphml.getGraph().getNode()::add, this::allocateNodeId);
        
        List<Edge> edges = graphml.getGraph().getEdge();
		edgeHandler = EdgeHandler.of(edges::add, this::determineNodeId);
    }
    
    private String allocateNodeId(UmlClass umlClass) {
		int nodeKey = getNodeKey(umlClass);
		if (nodes.containsKey(nodeKey)) {
			return null;
		}
		
		String nodeId = "n" + nodes.size();
		nodes.put(nodeKey, nodeId);
		
		return nodeId;
	}

	private void append(UmlClass umlClass) {
    	classHandler.append(umlClass);
    }
	
	private void append(UmlEdge umlEdge) {
    	edgeHandler.append(umlEdge);
    }

    private String determineNodeId(UmlClass umlClass) {
		int key = getNodeKey(umlClass);        
        if (!nodes.containsKey(key)) {
        	append(umlClass);
        }            
        return nodes.get(key);
	}

	private int getNodeKey(UmlClass umlClass) {
		return umlClass.getName().hashCode();
	}
}
