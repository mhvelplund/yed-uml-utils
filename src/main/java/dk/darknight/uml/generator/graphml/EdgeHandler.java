package dk.darknight.uml.generator.graphml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import org.graphdrawing.graphml.xmlns.Data;
import org.graphdrawing.graphml.xmlns.Edge;

import com.google.common.annotations.VisibleForTesting;
import com.yworks.xml.graphml.*;

import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlEdge;
import dk.darknight.uml.model.UmlEdge.EdgeType;

@Slf4j
@Getter(AccessLevel.NONE)
@Value(staticConstructor = "of")
class EdgeHandler implements UmlEntityHandler<UmlEdge> {
	private final Map<Integer, String> edges = new HashMap<Integer, String>();
	private final Consumer<Edge> addEdge;
	private final Function<UmlClass, String> getNodeId;

	@Override
	public void append(UmlEdge umlEdge) {
		if (edges.containsKey(umlEdge.hashCode())) {
			log.debug("GraphGenerator.append({}): Skipping known edge '{}'",
					umlEdge.hashCode(), umlEdge.getText().orElse("?"));
			return;

		}
		String edgeId = "e" + edges.size();
		edges.put(umlEdge.hashCode(), edgeId);

		LineStyle lineStyle = new LineStyle();

		Arrows arrows = new Arrows();

		switch (umlEdge.getType()) {
			case SRC_EXTENDED_BY_DST:
				arrows.setSource(ArrowType.WHITE_DELTA);
				arrows.setTarget(ArrowType.NONE);
				lineStyle.setType(LineType.LINE);
				break;
			case SRC_HAS_MEMBER_OF_DST_TYPE:
				arrows.setSource(ArrowType.NONE);
				arrows.setTarget(ArrowType.PLAIN);
				lineStyle.setType(LineType.LINE);
				break;
			case SRC_IMPLEMENTED_BY_DST:
				arrows.setSource(ArrowType.WHITE_DELTA);
				arrows.setTarget(ArrowType.NONE);
				lineStyle.setType(LineType.DASHED);
				break;
			default:
				throw new IllegalArgumentException("Unexpected edge type: "
						+ umlEdge.getType());
		}

		BendStyle bendStyle = new BendStyle();
		bendStyle.setSmoothed(true);

		PolyLineEdge polyLineEdge = new PolyLineEdge();
		polyLineEdge.setLineStyle(lineStyle);
		polyLineEdge.setArrows(arrows);
		polyLineEdge.setBendStyle(bendStyle);

		if (EdgeType.SRC_HAS_MEMBER_OF_DST_TYPE.equals(umlEdge.getType())) {
			List<EdgeLabel> labels = polyLineEdge.getEdgeLabel();

			String text = umlEdge.getText().orElse(null);
			String count = umlEdge.getCount().orElse(null);

			if (text != null) {
				labels.add(getLabel(text, "thead"));
			}

			if (count != null) {
				labels.add(getLabel(count, "ttail"));
			}
		}

		Data data = new Data();
		data.setKey(GraphGenerator.EDGE_KEY_ID);
		data.setPolyLineEdge(polyLineEdge);

		Edge edge = new Edge();
		edge.setId(edgeId);
		edge.setSource(getNodeId.apply(umlEdge.getSrc()));
		edge.setTarget(getNodeId.apply(umlEdge.getDst()));
		edge.setData(data);

		addEdge.accept(edge);
	}

	@VisibleForTesting
	EdgeLabel getLabel(String text, String modelPostion) {
		EdgeLabel label = new EdgeLabel();
		label.setModelName("six_pos");
		label.setModelPosition(modelPostion);
		label.setVisible(true);
		label.getContent().add(text);

		PreferredPlacementDescriptor ppd = new PreferredPlacementDescriptor();
		ppd.setPlacement("target");
		ppd.setSide("left|right");
		ppd.setSideReference("relative_to_edge_flow");

		label.getContent().add(ppd);

		return label;
	}
}