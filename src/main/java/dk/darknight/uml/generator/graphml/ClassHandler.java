package dk.darknight.uml.generator.graphml;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.function.Consumer;
import java.util.function.Function;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import org.graphdrawing.graphml.xmlns.Data;
import org.graphdrawing.graphml.xmlns.Node;

import com.google.common.annotations.VisibleForTesting;
import com.yworks.xml.graphml.Geometry;
import com.yworks.xml.graphml.NodeLabel;
import com.yworks.xml.graphml.UML;
import com.yworks.xml.graphml.UMLClassNode;

import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlClass.Stereotype;

@Slf4j
@Getter(AccessLevel.NONE) 
@Value(staticConstructor = "of")
class ClassHandler implements UmlEntityHandler<UmlClass> {
	private static final String NL = "\n";
	private final Consumer<Node> addNode;
	private final Function<UmlClass, String> allocateNodeIdKey;

	@Override
	public void append(UmlClass umlClass) {
		String nodeId = allocateNodeIdKey.apply(umlClass);
		if (nodeId == null) {
			log.trace("GraphGenerator.append(): Skipping known node '{}'", umlClass.getName());
			return;
		}

		int height = calcNodeHeight(umlClass);
		int width = calcWidth(umlClass);

		Geometry geometry = new Geometry();
		geometry.setHeight(BigDecimal.valueOf(height));
		geometry.setWidth(BigDecimal.valueOf(width));

		NodeLabel nodeLabel = new NodeLabel();
		nodeLabel.setAlignment("center");
		nodeLabel.setAutoSizePolicy("content");
		nodeLabel.setFontFamily("Dialog");
		nodeLabel.setFontSize(BigInteger.valueOf(13));

		final Stereotype stereotype = umlClass.getStereotype().orElse(null);

		if (Stereotype.ABSTRACT.equals(stereotype)) {
			nodeLabel.setFontStyle("bolditalic");
		} else {
			nodeLabel.setFontStyle("bold");
		}

		nodeLabel.setFontStyle("bold");
		nodeLabel.setModelName("custom");
		nodeLabel.setVisible(true);
		nodeLabel.getContent().add(umlClass.getName());

		UML uml = new UML();
		uml.setClipContent(true);
		uml.setAttributeLabel(umlClass.getFields().orElse(null));
		uml.setMethodLabel(umlClass.getMethods().orElse(null));

		if (Stereotype.INTERFACE.equals(stereotype)) {
			uml.setStereotype("interface");
		} else if (Stereotype.ENUMERATION.equals(stereotype)) {
			uml.setStereotype("enumeration");
		}

		UMLClassNode ucn = new UMLClassNode();
		ucn.setGeometry(geometry);
		ucn.setNodeLabel(nodeLabel);
		ucn.setUML(uml);

		Data data = new Data();
		data.setKey(GraphGenerator.NODE_KEY_ID);
		data.setUMLClassNode(ucn);

		Node node = new Node();
		node.setData(data);
		node.setId(nodeId);

		addNode.accept(node);
	}

	@VisibleForTesting
	int calcWidth(UmlClass umlClass) {
		int width = umlClass.getName().length();

		if (Stereotype.INTERFACE.equals(umlClass.getStereotype().orElse(null))) {
			width = Math.max(width, "<< interface >>".length());
		} else if (Stereotype.ENUMERATION.equals(umlClass.getStereotype().orElse(null))) {
            width = Math.max(width, "<< enumeration >>".length());
        }

		if (umlClass.getFields().isPresent()) {
			for (String string : umlClass.getFields().get().split(NL)) {
				width = Math.max(width, string.length());
			}
		}

		if (umlClass.getMethods().isPresent()) {
			for (String string : umlClass.getMethods().get().split(NL)) {
				width = Math.max(width, string.length());
			}
		}

		return width * 8;
	}

	@VisibleForTesting
	int calcNodeHeight(final UmlClass umlClass) {
		int height = 30;

		if (Stereotype.INTERFACE.equals(umlClass.getStereotype().orElse(null))) {
			height += 20;
		} else if (Stereotype.ENUMERATION.equals(umlClass.getStereotype()
				.orElse(null))) {
			height += 20;
		}

		if (umlClass.getFields().isPresent()) {
			int lines = umlClass.getFields().get().split(NL).length;
			height += (20 * lines);
		} else if (umlClass.getMethods().isPresent()) {
			height += 20;
		}

		if (umlClass.getMethods().isPresent()) {
			int lines = umlClass.getMethods().get().split(NL).length;
			height += (20 * lines);
		}

		return height;
	}
}