package dk.darknight.uml.generator.graphml;

import org.graphdrawing.graphml.xmlns.Graphml;

import dk.darknight.uml.model.UmlEntity;

/**
 * Handler implementations can append an entity of type {@code E} to a
 * {@link Graphml}-graph.
 *
 * @param <E>
 *            an extension of {@link UmlEntity}
 */
interface UmlEntityHandler<E extends UmlEntity> {
	void append(E umlEntity);
}
