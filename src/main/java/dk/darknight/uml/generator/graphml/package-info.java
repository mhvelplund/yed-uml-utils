/**
 * This package contains generator that creates a yEd compatible GraphML file 
 * based on a list of {@link dk.darknight.uml.model.UmlEntity UmlEntities}.
 * 
 * The central class is the {@link dk.darknight.uml.generator.graphml.GraphGenerator}-class.
 */
package dk.darknight.uml.generator.graphml;