/**
 * This package contains generator that creates a meta-model of 
 * {@link dk.darknight.uml.model.UmlEntity UmlEntities} from a set of 
 * {@link java.lang.Class}-objects.
 * 
 * The central class is the {@link dk.darknight.uml.generator.uml.UmlGenerator}-class.
 */
package dk.darknight.uml.generator.uml;