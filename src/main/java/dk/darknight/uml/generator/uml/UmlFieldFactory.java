package dk.darknight.uml.generator.uml;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Optional;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import dk.darknight.uml.model.UmlField;
import dk.darknight.uml.model.UmlField.Visibility;

/**
 * This factory encapsulates the logic related to mapping a Java {@link Field}
 * to a {@link UmlField} model.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class UmlFieldFactory {
    /**
     * Creates an UML model of a {@link Field}.
     * 
     * @param field
     *            the source field
     * @return a field model
     */
    public static UmlField umlFieldFrom(Field field) {
        Optional<String> mult = Optional.empty();

        int modifiers = field.getModifiers();
        Visibility vis;

        if (Modifier.isPublic(modifiers)) {
            vis = Visibility.PUBLIC;
        } else if (Modifier.isPrivate(modifiers)) {
            vis = Visibility.PRIVATE;
        } else if (Modifier.isProtected(modifiers)) {
            vis = Visibility.PROTECTED;
        } else {
            vis = Visibility.DEFAULT;
        }

        return UmlField.of(field.getName(), mult, field.getType(), vis,
                field.isEnumConstant());
    }
}