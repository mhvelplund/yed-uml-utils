package dk.darknight.uml.generator.uml;

import static dk.darknight.util.ClassUtils.getTypeName;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.google.common.collect.Lists;

import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlClass.Stereotype;
import dk.darknight.uml.model.UmlEdge;
import dk.darknight.uml.model.UmlField;

/**
 * This factory encapsulates the logic related to mapping a Java {@link Class}
 * to a {@link UmlClass} model.
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class UmlClassFactory {
    /**
     * Build a list of field models.
     * 
     * @param isEnumeration
     *            <code>true</code>, if the fields represent enum constants,
     *            <code>false</code> for normal class members
     * @param declaredFields
     *            the list for {@link Class#getDeclaredFields() declaredFields}
     *            in the parent class
     * @return a list of models
     */
    private static List<UmlField> getFields(boolean isEnumeration,
            Field[] declaredFields) {
        List<UmlField> fields = new ArrayList<>();

        if (declaredFields.length > 0) {
            Stream<Field> stream = Arrays.stream(declaredFields);

            if (isEnumeration) {
                stream = stream.filter((t) -> t.isEnumConstant());
            } else {
                stream = stream.filter((t) -> !t.getName().startsWith("$"));
            }

            fields = stream.map(f -> UmlFieldFactory.umlFieldFrom(f)).collect(
                    Collectors.toList());
        }
        return fields;
    }

    /**
     * Determine a class' UML stereotype.
     * 
     * @param clazz
     *            a class to inspect
     * @return an optional {@link Stereotype}
     */
    private static final Stereotype getStereotype(Class<?> clazz) {
        Stereotype stereotype = null;

        if (clazz.isInterface()) {
            stereotype = Stereotype.INTERFACE;
        } else if (Modifier.isAbstract(clazz.getModifiers())) {
            stereotype = Stereotype.ABSTRACT;
        } else if (clazz.isEnum()) {
            stereotype = Stereotype.ENUMERATION;
        }

        return stereotype;
    }

    /**
     * Helper method that creates a UML representation of a class member method.
     * 
     * @param method
     *            a member method
     * @return a string representation
     */
    private static String methodToString(Method method) {
        String methodName = "";
        int modifiers = method.getModifiers();

        if (Modifier.isPublic(modifiers)) {
            methodName += "+ ";
        } else if (Modifier.isProtected(modifiers)) {
            methodName += "# ";
        } else if (Modifier.isPrivate(modifiers)) {
            methodName += "- ";
        }

        String parameters = Arrays.stream(method.getParameters())
                .map((t) -> t.getName() + " : " + getTypeName(t.getType()))
                .collect(Collectors.joining(", "));

        methodName += MessageFormat.format("{0}({1}) : {2}", method.getName(),
                parameters, getTypeName(method.getReturnType()));

        return methodName;
    }

    /**
     * Create a stub model containing only a class name.
     * <p/>
     * Stubs only contain the name and stereotype of the source. Their sole
     * purpose is to function as the end-point of an {@link UmlEdge}.
     * 
     * @param clazz
     *            the source class
     * @return a UML model stub
     */
    public static UmlClass stubFrom(Class<?> clazz) {
        return UmlClass.of(clazz.getName(),
                Optional.ofNullable(getStereotype(clazz)), Optional.empty(),
                Optional.empty(), new ArrayList<>(), new ArrayList<>(), true);
    }

    /**
     * Create a detailed model of a Java class.
     * 
     * @param clazz
     *            a class
     * @return a model
     */
    public static UmlClass umlClassFrom(Class<?> clazz) {
        Stereotype stereotype = getStereotype(clazz);

        Field[] declaredFields = {};

        try {
            declaredFields = clazz.getDeclaredFields();
        } catch (NoClassDefFoundError e) {
            // This happens if there are dependencies that are not in the class
            // path
            log.warn(
                    "Can't model member fields for for '{}'. Can't find '{}'.",
                    clazz.getName(), e.getMessage().replace('/', '.'));
        }

        List<UmlField> fieldTypes = getFields(
                Stereotype.ENUMERATION.equals(stereotype), declaredFields);

        Optional<String> methods = Optional.empty();
        Optional<Class<?>> extensionOf = Optional.empty();
        List<Class<?>> implementationOf = new ArrayList<>();

        if (!Stereotype.ENUMERATION.equals(stereotype)) {
            Method[] declaredMethods = {};

            try {
                declaredMethods = clazz.getDeclaredMethods();
            } catch (NoClassDefFoundError e) {
                // This happens if there are dependencies that are not in the
                // class path
                log.warn(
                        "Can't model member methods for for '{}'. Can't find '{}'.",
                        clazz.getName(), e.getMessage().replace('/', '.'));
            }

            if (declaredMethods.length > 0) {
                methods = Optional.of(Arrays.stream(declaredMethods)
                        .filter((t) -> !t.getName().startsWith("$"))
                        .map(UmlClassFactory::methodToString)
                        .collect(Collectors.joining("\n")));
            }

            Class<?> sc = clazz.getSuperclass();

            if (Object.class.equals(sc)) {
                extensionOf = Optional.empty();
            } else {
                // Why can 'sc' be null?
                extensionOf = Optional.ofNullable(sc);
            }

            implementationOf = Lists.newArrayList(clazz.getInterfaces());
        }
        return UmlClass.of(clazz.getName(), Optional.ofNullable(stereotype),
                methods, extensionOf, implementationOf, fieldTypes, false);
    }
}