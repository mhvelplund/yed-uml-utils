package dk.darknight.uml.generator.uml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlEdge;
import dk.darknight.uml.model.UmlEntity;
import dk.darknight.uml.model.UmlField;
import static dk.darknight.util.ClassUtils.classMatchesFilter;

/**
 * A singleton factory class that can create a UML meta model in the form of a
 * list of {@link UmlClass} and {@link UmlEdge} objects based on a list of Java
 * classes.
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UmlGenerator {
    public static List<UmlEntity> getUml(Set<Class<?>> classes, Set<String> packageFilter) {
        /*
         * Build a map of classes. The list of classes under the "true" key
         * should have full UmlClass definitions, the ones under false should
         * have stubs (because they don't match the filter).
         */
        final Map<Class<?>, UmlClass> umlClasses = classes.stream()
                .collect(Collectors.toConcurrentMap(
                        Function.identity(),
                        c -> classMatchesFilter(c, packageFilter) ? 
                                UmlClassFactory.umlClassFrom(c) : 
                                UmlClassFactory.stubFrom(c)));

        List<UmlEntity> entities= new ArrayList<>();
        
        // Calculate edges containing an un-stubbed UML class. Edges from stub to stub will not be included
        List<UmlEdge> edges = umlClasses.values().stream()
                .filter(c -> !c.isStub()) // find non-stubbed UML models                
                .map(umlClass -> {
                    List<UmlEdge> outboundEdges = new ArrayList<UmlEdge>();

                    addSuperClassEdge(umlClasses, umlClass, outboundEdges);
                    addInterfaceEdges(umlClasses, umlClass, outboundEdges);
                    addMemberVariables(umlClasses, umlClass, outboundEdges);

                    return outboundEdges;
                }) // map to a list of edges
                .collect(Collectors.reducing((t, u) -> {
                    t.addAll(u);
                    return t;
                })) // collect and flatten into a single list of edges
                .orElse(new ArrayList<UmlEdge>());
        
        if (log.isTraceEnabled()) {
            edges.stream().forEach(
                    e -> log.trace("{}: {} ({}) -> {} ({})", e.getType(), 
                            e.getSrc().getName(), e.getSrc().isStub(), 
                            e.getDst().getName(), e.getDst().isStub()));
        }
               
        // Remove stubs that have no edges
        List<Class<?>> orphans = new ArrayList<>();
        umlClasses.forEach((k,v) -> {
            if (v.isStub()) {
                boolean isOrphan = ! edges.stream().anyMatch(e -> e.getSrc().equals(v) || e.getDst().equals(v));
                if (isOrphan) orphans.add(k);
            }
        });
        
        if (log.isTraceEnabled()) {
            orphans.stream()
            .sorted((c1, c2) -> c1.getName().compareTo(c2.getName()))
            .forEach(o -> {
                log.trace("Removing orphan stub: '{}'", o.getName());
            });
        }

        orphans.stream().forEach(o -> umlClasses.remove(o));
        
        // Return the remaining UML classes and edges
        entities.addAll(umlClasses.values());
        
        if (log.isDebugEnabled()) {
            umlClasses.values().stream().sorted((c1,c2) -> c1.getName().compareTo(c2.getName())).forEach(uc -> {
                log.debug("Modelling class '{}' {}", uc.getName(), uc.isStub() ? "(S)" : "");
            });
        }
        
        entities.addAll(edges);
        
        return entities;
    }

    /**
     * Find out which interfaces we implement.
     * <p>
     * Link the {@code umlClass} to it's implemented interfaces.
     * </p>
     * @param umlClasses
     *            a map of {@link Class} to {@link UmlClass} bindings
     * @param umlClass
     *            the implementing class
     * @param outboundEdges
     *            a list of out-bound edges to append any interfaces to
     */
    private static void addInterfaceEdges(final Map<Class<?>, UmlClass> umlClasses,
            UmlClass umlClass, List<UmlEdge> outboundEdges) {
        if (umlClass.getImplementationOf().size() > 0) {
            for (Class<?> iface : umlClass.getImplementationOf()) {
                if (!umlClasses.containsKey(iface)) {
                    umlClasses.put(iface,
                            UmlClassFactory.stubFrom(iface));
                }

                UmlClass implementedIFace = umlClasses.get(iface);

                outboundEdges.add(UmlEdgeFactory.implementation(
                        implementedIFace, umlClass));
            }
        }
    }

    /**
     * Find members.
     *
     * @param umlClasses
     *            a map of {@link Class} to {@link UmlClass} bindings
     * @param umlClass
     *            the class with members
     * @param outboundEdges
     *            a list of out-bound edges to append any interfaces to
     */
    private static void addMemberVariables(final Map<Class<?>, UmlClass> umlClasses,
            UmlClass umlClass, List<UmlEdge> outboundEdges) {
        List<UmlField> fieldTypes = umlClass.getFieldTypes();
        List<UmlField> linkedFields = new ArrayList<UmlField>();
        
        for (UmlField field : fieldTypes) {
            if (field.isEnumConstant()) {
                continue;
            }
            
            Class<?> type = field.getType();
            if (umlClasses.containsKey(type)) {
                UmlClass memberVar = umlClasses.get(type);
                outboundEdges.add(UmlEdgeFactory.hasMember(umlClass, memberVar, field.getName(), field.getCount().orElse(null)));
                linkedFields.add(field);
            }
        }
        
        if (log.isTraceEnabled()) {
            linkedFields.stream()
            .sorted((c1, c2) -> c1.getName().compareTo(c2.getName()))
            .forEach(f -> {
                log.trace("Removing linked field: '{}.{}'", umlClass.getName(), f.getName());
            });
        }
        
        fieldTypes.removeAll(linkedFields);
    }

    /**
     * Find out if this class has a super class.
     * <p>
     * Link the {@code umlClass} to it's super class, if it has one.
     * </p>
     * 
     * @param umlClasses
     *            a map of {@link Class} to {@link UmlClass} bindings
     * @param umlClass
     *            the child class
     * @param outboundEdges
     *            a list of out-bound edges to append any super class to
     */
    private static void addSuperClassEdge(final Map<Class<?>, UmlClass> umlClasses,
            UmlClass umlClass, List<UmlEdge> outboundEdges) {
        if (umlClass.getExtensionOf().isPresent()) {
            Class<?> parent = umlClass.getExtensionOf().get();

            if (!umlClasses.containsKey(parent)) {
                umlClasses.put(parent, UmlClassFactory.stubFrom(parent));
            }

            UmlClass parentUmlClass = umlClasses.get(parent);

            outboundEdges.add(UmlEdgeFactory.extension(parentUmlClass, umlClass));
        }
    }

}