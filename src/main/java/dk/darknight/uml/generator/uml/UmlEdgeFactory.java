package dk.darknight.uml.generator.uml;

import java.util.Optional;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlEdge;
import dk.darknight.uml.model.UmlEdge.EdgeType;

/**
 * This factory encapsulates the logic related to representing a relationship
 * between to {@link UmlClass}es as a {@link UmlEdge} model.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class UmlEdgeFactory {
    /**
     * Short-cut constructor for an extension relationship.
     * 
     * @param parent
     *            the super class
     * @param child
     *            the child class
     * @return an edge model
     */
    public static UmlEdge extension(UmlClass parent, UmlClass child) {
        return UmlEdge.of(parent, child, Optional.empty(), Optional.empty(),
                EdgeType.SRC_EXTENDED_BY_DST);
    }

    /**
     * Short-cut constructor for a member field relationship.
     * 
     * @param classWithMember
     *            the class that has a member
     * @param memberType
     *            the target type of that member
     * @param memberName
     *            then name of the member variable
     * @return an edge model
     */
    public static UmlEdge hasMember(UmlClass classWithMember,
            UmlClass memberType, String memberName) {
        return hasMember(classWithMember, memberType, memberName, null);
    }

    /**
     * Short-cut constructor for a member field relationship.
     * 
     * @param classWithMember
     *            the class that has a member
     * @param memberType
     *            the target type of that member
     * @param memberName
     *            then name of the member variable
     * @param count
     *            the multiplicity of the relation ship
     * @return an edge model
     */
    public static UmlEdge hasMember(UmlClass classWithMember,
            UmlClass memberType, String memberName, String count) {
        return UmlEdge
                .of(classWithMember, memberType, Optional.of(memberName),
                        Optional.ofNullable(count),
                        EdgeType.SRC_HAS_MEMBER_OF_DST_TYPE);
    }

    /**
     * Short-cut constructor for an implementation relationship.
     * 
     * @param iface
     *            the interface
     * @param impl
     *            the implementor
     * @return an edge model
     */
    public static UmlEdge implementation(UmlClass iface, UmlClass impl) {
        return UmlEdge.of(iface, impl, Optional.empty(), Optional.empty(),
                EdgeType.SRC_IMPLEMENTED_BY_DST);
    }
}