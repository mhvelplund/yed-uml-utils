/**
 * This package contains the model objects that represent UML entities.
 */
package dk.darknight.uml.model;