package dk.darknight.uml.model;

import java.util.Optional;

import lombok.NonNull;
import lombok.Value;

/**
 * Model object representing a relation between to {@link UmlClass}es.
 */
@Value(staticConstructor = "of")
public class UmlEdge implements UmlEntity {
    /** Relation types. */
    public static enum EdgeType {
        /** The source is an interface implemented by the destination. */
        SRC_IMPLEMENTED_BY_DST,

        /** The source is the super class of the destination. */
        SRC_EXTENDED_BY_DST,

        /**
         * The source has a member field with the type of the destination class.
         */
        SRC_HAS_MEMBER_OF_DST_TYPE
    }

    /** The source class. */
    @NonNull UmlClass src;

    /** The destination class. */
    @NonNull UmlClass dst;

    /** A label shown next to the edge. */
    @NonNull Optional<String> text;

    /** The multiplicity of the relation. */
    @NonNull Optional<String> count;

    /** The type of relation. */
    @NonNull EdgeType type;
}
