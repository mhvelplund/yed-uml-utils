package dk.darknight.uml.model;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.NonNull;
import lombok.Value;

/**
 * Model object representing a class, interface, or enumeration.
 */
@Value(staticConstructor = "of")
public class UmlClass implements UmlEntity {
	/** UML stereotype. */
	public static enum Stereotype {
		/** This object is an interface. */
		INTERFACE,

		/** This is an abstract class. */
		ABSTRACT,

		/** This is an enumeration. */
		ENUMERATION
	}

	/** Class name. */
	@NonNull String name;

	/** UML stereotype. */
	@NonNull Optional<Stereotype> stereotype;

	/** A newline separated list of UML method descriptors. */
	@NonNull Optional<String> methods;

	/** The super class of this class, if different from {@link Object}. */
	@NonNull Optional<Class<?>> extensionOf;

	/** The interface implemented by this class. */
	@NonNull List<Class<?>> implementationOf;

	/**
	 * A model of the member variables.
	 * 
	 * @see #fields
	 */
	@NonNull List<UmlField> fieldTypes;
	
	/** Is this a stub model? */
	boolean stub;

	/**
	 * Get an UML string representation of the {@link #fieldTypes}.
	 * 
	 * @return a newline separated list of UML field descriptors, or
	 *         {@link Optional#empty()}.
	 */
	public Optional<String> getFields() {
		Optional<String> fields;

		if (fieldTypes.size() > 0) {
			fields = Optional.of(fieldTypes.stream().map(UmlField::toString)
					.collect(Collectors.joining("\n")));
		} else {
			fields = Optional.empty();
		}

		return fields;
	}

}