package dk.darknight.uml.model;

import static dk.darknight.util.ClassUtils.getTypeName;

import java.text.MessageFormat;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;

/**
 * Model object representing a member field of an {@link UmlClass}.
 */
@Value(staticConstructor = "of")
public class UmlField {
	
	@AllArgsConstructor
	public static enum Visibility {
		PUBLIC('+'),
		PRIVATE('-'),
		PROTECTED('*'),
		DEFAULT(' ');
		
		@Getter
		private final char visibility;
	}
	
	/** The field name. */
	@NonNull String name;

	/** The multiplicity of the field. */
	@NonNull Optional<String> count;

	/** The type of the field. */
	@NonNull Class<?> type;
	
	/** The fields visibility. */
	@NonNull Visibility visibility;
	
	/** <code>true</code>, if the field is actually an enumeration constant. */
	boolean enumConstant;
	
	/**
	 * Generates the UML representation.
	 * 
	 * @return a string representation, e.g.:
	 *         <p/>
	 *         {@code + name : String}
	 */
	public String toString() {
		String string;

		if (isEnumConstant()) {
			string = name;
		} else {
			string = MessageFormat.format("{0} {1} : {2}",
					visibility.getVisibility(), name, getTypeName(type));
		}
		
		return string;
	}
}
