package dk.darknight.util;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.Consumer;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Helper class that provides a visitor implementation for class loading.
 * <p>
 * Default behavior is to ignore any visitation errors, and to load call the {@link #addClass} 
 * {@link Consumer#accept(Object) accept}-method for all classes found and loaded using the the 
 * provided {@link #classLoader}.
 * </p>
 * 
 * @see ClassUtils
 */
@Slf4j
@AllArgsConstructor
class ClassFinderVisitor extends SimpleFileVisitor<Path> {
    /** The class loader used for finding classes. */
    final private ClassLoader classLoader;
    
    /** A method reference that can add a loaded class to an external data structure. */
    final private Consumer<Class<?>> addClass;
    
    /** The number of elements to strip of the beginning of the path to find the class name. */
    final private int pathOffset;

    /** {@inheritDoc} */
    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
        Path file = path.subpath(pathOffset, path.getNameCount());
        if (file.getFileName().toString().toLowerCase().endsWith(".class")) {
            String fileName = file.toString();
            String className = fileName.substring(0, fileName.length() - 6).replace("/", ".").replace("\\", ".");
            log.trace("Loading '{}'", className);
            try {
                Class<?> clazz = classLoader.loadClass(className);
                addClass.accept(clazz);
            } catch (IllegalAccessError | NoClassDefFoundError | ClassNotFoundException | SecurityException e) {
                // Usually related to woven aspects or badly signed jars ... just ignore it
                log.debug("[{}] Can't find class definition for '{}'. Ignoring.", e.getClass().getSimpleName(), className);
            }
        }
        return FileVisitResult.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc)
            throws IOException {
        log.error("{}: {} - {}", exc.getClass().getSimpleName(), file, exc.getMessage());
        return FileVisitResult.CONTINUE;
    }

    /** {@inheritDoc} */
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc)
            throws IOException {
        if (exc != null) {
            log.error("{}: {} - {}", exc.getClass().getSimpleName(), dir + "", exc.getMessage());
            return FileVisitResult.SKIP_SUBTREE;
        } else {
            return FileVisitResult.CONTINUE;
        }
    }
}