package dk.darknight.util;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.google.common.base.Throwables;

@Slf4j
@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class ClassUtils {
    /**
     * Recursive method used to find all classes in a set of archives and directories.
     * 
     * @param baseDirs
     *            a set of JARs and class directories
     * @return a set of classes found
     */
    public static Set<Class<?>> findClasses(Set<File> baseDirs) {
        try {
            List<URL> classPath = new ArrayList<URL>();
            for (File directory : baseDirs) {
                classPath.add(directory.toURI().toURL());
            }

            ClassLoader classLoader = new URLClassLoader(classPath.toArray(new URL[] {}));
            Set<Class<?>> classes = new HashSet<Class<?>>();
            
            FileVisitor<Path> classFinder;

            for (File directory : baseDirs) {
                Path path;
                
                if (directory.isFile()) {
                    // Archives (JAR, etc.)
                    path = FileSystems.newFileSystem(directory.toPath(), null).getPath("/");
                    classFinder = new ClassFinderVisitor(classLoader, classes::add, 0);
                } else {
                    // Class directories
                    path = directory.toPath();
                    classFinder = new ClassFinderVisitor(classLoader, classes::add, path.getNameCount());
                }
                
                Files.walkFileTree(path, classFinder);
            }

            return classes;
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }
    
	/**
	 * Safely extract the {@link Class#getSimpleName() simpleName} of a class.
	 * <p>
	 * Sometimes getting the "simpleName" of a class causes an exception. In
	 * this method we try to trap those exceptions and make educated guesses
	 * about the class' name.
	 * </p>
	 * 
	 * @param type
	 *            a class that may, or may not, be correctly loaded.
	 * @return a class name
	 */
	public static String getTypeName(Class<?> type) {
		String simpleName;
		try {
			simpleName = type.getSimpleName();
		} catch (NoClassDefFoundError e) {
			String[] parts = e.getMessage().split("/");
			simpleName = parts[parts.length - 1];
			log.debug(
					"[NoClassDefFoundError] can't get simple name for '{}'. Guessing '{}'",
					type, simpleName);
		} catch (IncompatibleClassChangeError e) {
			String[] parts = type.toString().split("\\.");
			simpleName = parts[parts.length - 1];
			log.debug("[{}] can't get simple name for '{}'. Guessing '{}'", e
					.getClass().getSimpleName(), type, simpleName);
		}

		return simpleName;
	}

    /**
     * Check if a class falls within a given package name filter.
     * 
     * @param clazz
     *            a class
     * @param packageFilter
     *            a set of string prefixes used to filter class packages. A
     *            <code>null</code> or empty set matches <em>all</em> classes
     * @return <code>true</code>, if the class matches one or more of the given
     *         prefixes
     */
    public static boolean classMatchesFilter(final Class<?> clazz,
            final Set<String> packageFilter) {
        return packageFilter == null
                || packageFilter.isEmpty()
                || packageFilter.stream().anyMatch(
                        f -> clazz.getName().startsWith(f));
    }
}