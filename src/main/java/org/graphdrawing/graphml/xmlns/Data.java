//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.09.16 at 03:37:30 PM CEST 
//

package org.graphdrawing.graphml.xmlns;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.yworks.xml.graphml.PolyLineEdge;
import com.yworks.xml.graphml.UMLClassNode;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://www.yworks.com/xml/graphml}PolyLineEdge"/>
 *         &lt;element ref="{http://www.yworks.com/xml/graphml}UMLClassNode"/>
 *       &lt;/choice>
 *       &lt;attribute name="key" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "polyLineEdge", "umlClassNode" })
@XmlRootElement(name = "data")
public class Data {

    @XmlElement(name = "PolyLineEdge", namespace = "http://www.yworks.com/xml/graphml")
    protected PolyLineEdge polyLineEdge;
    @XmlElement(name = "UMLClassNode", namespace = "http://www.yworks.com/xml/graphml")
    protected UMLClassNode umlClassNode;

    @XmlAttribute(name = "key", required = true) 
//    @XmlIDREF
//    @XmlSchemaType(name = "IDREF")
    protected String key;

    /**
     * Gets the value of the polyLineEdge property.
     * 
     * @return possible object is {@link PolyLineEdge }
     * 
     */
    public PolyLineEdge getPolyLineEdge() {
        return polyLineEdge;
    }

    /**
     * Sets the value of the polyLineEdge property.
     * 
     * @param value
     *            allowed object is {@link PolyLineEdge }
     * 
     */
    public void setPolyLineEdge(PolyLineEdge value) {
        this.polyLineEdge = value;
    }

    /**
     * Gets the value of the umlClassNode property.
     * 
     * @return possible object is {@link UMLClassNode }
     * 
     */
    public UMLClassNode getUMLClassNode() {
        return umlClassNode;
    }

    /**
     * Sets the value of the umlClassNode property.
     * 
     * @param value
     *            allowed object is {@link UMLClassNode }
     * 
     */
    public void setUMLClassNode(UMLClassNode value) {
        this.umlClassNode = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *            allowed object is {@link Object }
     * 
     */
    public void setKey(String value) {
        this.key = value;
    }

}
