package dk.darknight.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import dk.darknight.uml.UmlGeneratorTest;

public class ClassUtilsTest {
    private final List<String> expectedPackages = Lists.newArrayList(
            "dk.darknight.canary.sub.Child", "dk.darknight.canary.Parent",
            "dk.darknight.canary.Person", "dk.darknight.canary.HasName");

    @Test
    public void testClassDoesNotMatchesFilter() {
        assertFalse(ClassUtils.classMatchesFilter(UmlGeneratorTest.class, Sets.newHashSet("com.foo")));
    }

    @Test
    public void testClassMatchesEmptyFilter() {
        assertTrue(ClassUtils.classMatchesFilter(UmlGeneratorTest.class, Sets.newHashSet()));
    }    

    @Test
    public void testClassMatchesFilter() {
        assertTrue(ClassUtils.classMatchesFilter(UmlGeneratorTest.class, Sets.newHashSet("dk")));
    }
    
    @Test
    public void testClassMatchesNullFilter() {
        assertTrue(ClassUtils.classMatchesFilter(UmlGeneratorTest.class, null));
    }

    @Test
    public void testFile() throws IOException {
        // Run
        Set<Class<?>> classes = ClassUtils.findClasses(Sets.newHashSet(new File("src/test/resources/canary-classes")));

        // Assert
        assertTrue(classes.size() == expectedPackages.size());
        assertTrue(classes.stream().allMatch(c -> expectedPackages.contains(c.getName())));
    }

    @Test
    public void testGetTypeName() {
        assertEquals(ClassUtilsTest.class.getSimpleName(), ClassUtils.getTypeName(ClassUtilsTest.class));
    }

    @Test
    public void testJar() throws IOException {        
        //Run
        Set<Class<?>> classes = ClassUtils.findClasses(Sets.newHashSet(new File("src/test/resources/canary.jar")));
        
        // Assert
        assertTrue(classes.size() == expectedPackages.size());
        assertTrue(classes.stream().allMatch(c -> expectedPackages.contains(c.getName())));
    }

}