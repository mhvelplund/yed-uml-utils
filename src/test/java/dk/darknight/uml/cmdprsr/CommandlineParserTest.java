package dk.darknight.uml.cmdprsr;

import static dk.darknight.uml.cmdprsr.CommandlineParser.*;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.ParseException;
import org.junit.Test;

public class CommandlineParserTest {
	private static final String PS = File.pathSeparator;

	@Test(expected = MissingArgumentException.class)
	public void testEmptyClassPath() throws FileNotFoundException,
			ParseException {
		String[] arguments = { "-cp" };
		getParameters(arguments);
	}

	@Test
	public void testClassPath() throws FileNotFoundException, ParseException {
		String[] arguments = {
				"-cp",
				"src/test/resources/canary.jar" + PS
						+ "src/test/resources/canary-classes" + PS + "XXX" + PS
						+ "src/test/resources/canary.jar" };
		Parameters parameters = getParameters(arguments);
		Set<File> classPathEntries = parameters.getClassPathEntries();
		assertTrue(classPathEntries.size() == 2);
	}

	@Test
	public void testPrintHelp() throws FileNotFoundException, ParseException {
		String[] arguments = { "-?", "will be ignored" };
		Parameters parameters = getParameters(arguments);
		assertNull(parameters);
		// printHelp();
	}
	
	@Test
	public void testPackagePrefixes() throws FileNotFoundException, ParseException {
		String[] arguments = { "-p", "com.google" + PS + "dk.darknight", "-cp", "src" };
		Parameters parameters = getParameters(arguments);
		Set<String> packageFilter = parameters.getPackageFilter();
		assertTrue(packageFilter.size() == 2);
	}

	@Test
	public void mapStringsToFiles() {
		String[] fileNames = { 
				"src/test/resources/canary.jar",
				"src/test/resources/canary-classes",
				"src/test/resources/canary.jar", 
				"XXX" 
		};
		
		Set<File> files = fileNamesToFiles(fileNames);
		assertTrue(files.size() == 2);
		
		List<String> list = files.stream()
				.map(f -> f.getName())
				.sorted()
				.collect(Collectors.toList());
		
		assertEquals("canary-classes", list.get(0));
		assertEquals("canary.jar", list.get(1));
	}
}
