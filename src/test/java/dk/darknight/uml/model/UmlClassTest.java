package dk.darknight.uml.model;
import static org.junit.Assert.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

import com.google.common.collect.Lists;

import dk.darknight.uml.model.UmlField.Visibility;

public class UmlClassTest {

    @Test
    public void testGetFields() {
        UmlField pub = UmlField.of("pub", Optional.empty(), String.class, Visibility.PUBLIC, false);
        UmlField priv = UmlField.of("priv", Optional.empty(), Integer.class, Visibility.PRIVATE, false);
        UmlField def = UmlField.of("def", Optional.empty(), Boolean.class, Visibility.DEFAULT, false);
        UmlField prot = UmlField.of("prot", Optional.empty(), Long.class, Visibility.PROTECTED, false);
        List<UmlField> fieldTypes = Lists.newArrayList(pub, priv, def, prot);
        
        Optional<String> fields = UmlClass.of("foo", Optional.empty(), Optional.empty(), Optional.empty(), new ArrayList<>(), fieldTypes, false).getFields();

        boolean allMatch = fieldTypes.stream().allMatch(
                f -> {
                    String s = MessageFormat.format("{0} {1} : {2}", 
                            f.getVisibility().getVisibility(), 
                            f.getName(), 
                            f.getType().getSimpleName());

                    return fields.get().contains(s);
                });
        
        assertTrue(allMatch);
    }

}