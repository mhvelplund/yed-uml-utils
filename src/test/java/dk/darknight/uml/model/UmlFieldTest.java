package dk.darknight.uml.model;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;

import dk.darknight.uml.model.UmlField.Visibility;

public class UmlFieldTest {

    @Test
    public void testFieldVisibilityToString() {
        boolean allMatch = Arrays.stream(Visibility.values()).allMatch(v -> getField(v, false).toString().startsWith(String.valueOf(v.getVisibility())));
        assertTrue(allMatch);
    }

    @Test
    public void testToStringPattern() {
        assertEquals("+ foo : UmlFieldTest", getField(Visibility.PUBLIC, false).toString());
    }

    @Test
    public void testEnumToStringPattern() {
        assertEquals("foo", getField(Visibility.PUBLIC, true).toString());
    }

    private UmlField getField(Visibility v, boolean isEnum) {
        return UmlField.of("foo", Optional.empty(), UmlFieldTest.class, v, isEnum);
    }
    
}