package dk.darknight.uml;

import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.graphdrawing.graphml.xmlns.Graphml;
import org.junit.Test;

import com.google.common.collect.Sets;

import dk.darknight.uml.generator.graphml.GraphGenerator;
import dk.darknight.uml.generator.uml.UmlGenerator;
import dk.darknight.uml.model.UmlEntity;

public class UmlGeneratorTest {

	@Test
	public void testGetUml() throws JAXBException {
		@SuppressWarnings("unchecked")
		List<UmlEntity> uml = UmlGenerator.getUml(Sets.newHashSet(Graphml.class, GraphGenerator.class), new HashSet<>());
		
		Graphml diagram = GraphGenerator.generateUmlDiagram(uml);
		JAXBContext
				.newInstance(Graphml.class)
				.createMarshaller()
				.marshal(diagram, new OutputStreamWriter(System.out));
	}

}
