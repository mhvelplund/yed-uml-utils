package dk.darknight.uml.generator.uml;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.NonNull;
import lombok.Value;
import lombok.experimental.Accessors;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlEdge;
import dk.darknight.uml.model.UmlEntity;
import dk.darknight.util.ClassUtils;

/** Quick-and dirty tuple. */
@Accessors(fluent=true)
@Value(staticConstructor="of")
class Pair<A,B> {
    @NonNull A a;
    @NonNull B b;
}

/**
 * <h1>Canary contents</h1> Classes:
 * <ul>
 * <li>dk.darknight.canary.HasName</li>
 * <li>dk.darknight.canary.Parent</li>
 * <li>dk.darknight.canary.Person</li>
 * <li>dk.darknight.canary.sub.Child</li>
 * </ul>
 * Edges:
 * <ul>
 * <li>SRC_EXTENDED_BY_DST(dk.darknight.canary.Person -&gt; dk.darknight.canary.Parent)</li>
 * <li>SRC_IMPLEMENTED_BY_DST(dk.darknight.canary.HasName -&gt; dk.darknight.canary.sub.Child)</li>
 * <li>SRC_IMPLEMENTED_BY_DST(dk.darknight.canary.HasName -&gt; dk.darknight.canary.Person)</li>
 * <li>SRC_HAS_MEMBER_OF_DST_TYPE(dk.darknight.canary.sub.Child -&gt; dk.darknight.canary.Parent)</li>
 * <li>SRC_HAS_MEMBER_OF_DST_TYPE(dk.darknight.canary.sub.Child -&gt; dk.darknight.canary.Parent)</li>
 * </ul>
 */
public class UmlGeneratorTest {
    private Set<Class<?>> classes;
    private List<String> filteredClassNames;
    private List<String> allClassNames;
    private List<Pair<String,String>> filteredEdges;
    private List<Pair<String,String>> allEdges;
    
    @Before
    @SuppressWarnings("unchecked")
    public void setup() {
        classes = ClassUtils.findClasses(Sets.newHashSet(new File("src/test/resources/canary-classes")));
        
        filteredClassNames = Lists.newArrayList("HasName", "Parent", "Child");
        
        allClassNames = Lists.newArrayList(filteredClassNames);
        allClassNames.add("Person");
        
        filteredEdges = Lists.newArrayList(
                Pair.of("HasName", "Child"),
                Pair.of("Child", "Parent"), // mom
                Pair.of("Child", "Parent")); // dad
                
        allEdges = Lists.newArrayList(filteredEdges);
        allEdges.add(Pair.of("Person", "Parent"));
        allEdges.add(Pair.of("HasName", "Person"));
    }
    
    @Test
    public void testGetUmlUnfiltered() {        
        validateUml(allClassNames, allEdges, new HashSet<>());
    }

    @Test
    public void testGetUmlFiltered() {        
        validateUml(filteredClassNames, filteredEdges, Sets.newHashSet("dk.darknight.canary.sub"));
    }
    
    @Test
    public void testGetUmlFilteredWithEverything() {        
        validateUml(allClassNames, allEdges, Sets.newHashSet("dk.darknight"));
    }

    /**
     * Check that the generated matches the expected classes and edges.
     * 
     * @param classNames
     *            a list of expected class names
     * @param edges
     *            a list of expected edges
     * @param packageFilter
     *            the filter to use with {@link UmlGenerator#getUml(Set, Set)}
     * @return 
     * @throws AssertionError
     *             if the generated UML fails validation
     */
    private List<UmlEntity> validateUml(List<String> classNames,
            List<Pair<String, String>> edges, HashSet<String> packageFilter) 
    {
        List<UmlEntity> uml = UmlGenerator.getUml(classes, packageFilter);        
                
        List<UmlClass> umlClasses = filterAndCast(uml,UmlClass.class);
        assertEquals("Wrong number of classes included.", classNames.size(), umlClasses.size());
        assertTrue("Unexpected contents of class model.", umlClasses.stream().allMatch(c -> classNames.contains(getSimpleName(c.getName()))));

        List<UmlEdge> umlEdges = filterAndCast(uml,UmlEdge.class);
        assertEquals("Wrong number of edges included.", edges.size(), umlEdges.size());
        assertTrue("Unexpected contents of edge model.", umlEdges.stream().allMatch(e -> edges.contains(getPair(e))));
        
        return uml;
    }
    
    /** Make a pair out of the simple-names of the source and destination class. */
    private Pair<String,String> getPair(UmlEdge e) {
        return Pair.of(
            getSimpleName(e.getSrc().getName()), 
            getSimpleName(e.getDst().getName())
        );
    }

    /** Reduce a list to instances of a specific class. */
    private <T> List<T> filterAndCast(Collection<?> col, Class<T> t) {        
        return col.stream().filter(c -> t.isAssignableFrom(c.getClass()))
                .map(t::cast).collect(Collectors.toList());
    }
    
    /** Get the last part of a dot-separated class name. */
    private String getSimpleName(String name) {
        String[] parts = name.split("\\.");        
        return parts[parts.length-1];
    }

}
