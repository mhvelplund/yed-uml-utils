package dk.darknight.uml.generator.uml;

import static org.junit.Assert.*;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

import org.junit.Test;

import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlClass.Stereotype;

@Getter
abstract class MyParent {
    private String foo;
}

@Getter
@ToString
@SuppressWarnings("serial")
class MyChild extends MyParent implements Serializable {
    private String bar;
}

public class UmlClassFactoryTest {

    @Test
    public void testStubFrom() {
        UmlClass child = UmlClassFactory.stubFrom(MyChild.class);
        assertEquals(MyChild.class.getName(), child.getName());
        assertFalse(child.getMethods().isPresent());
        assertTrue(child.getFieldTypes().isEmpty());
        assertTrue(child.getImplementationOf().isEmpty());

        UmlClass parent = UmlClassFactory.stubFrom(MyParent.class);
        assertEquals(Stereotype.ABSTRACT, parent.getStereotype().get());
    }

    @Test
    public void testUmlClassFrom() {
        UmlClass child = UmlClassFactory.umlClassFrom(MyChild.class);
        assertEquals(MyParent.class, child.getExtensionOf().get());
        assertEquals("bar", child.getFieldTypes().get(0).getName());
        assertEquals(Serializable.class, child.getImplementationOf().get(0));
        assertEquals(2, child.getMethods().get().split("\n").length);
        assertEquals(MyChild.class.getName(), child.getName());
        assertFalse(child.getStereotype().isPresent());

        UmlClass parent = UmlClassFactory.umlClassFrom(MyParent.class);
        assertFalse(parent.getExtensionOf().isPresent());
        assertEquals("foo", parent.getFieldTypes().get(0).getName());
        assertTrue(parent.getImplementationOf().isEmpty());
        assertEquals(1, parent.getMethods().get().split("\n").length);
        assertEquals(MyParent.class.getName(), parent.getName());
        assertEquals(Stereotype.ABSTRACT, parent.getStereotype().get());
    }

}