package dk.darknight.uml.generator.graphml;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.graphdrawing.graphml.xmlns.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;

import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlClass.Stereotype;
import dk.darknight.uml.model.UmlField;
import dk.darknight.uml.model.UmlField.Visibility;

interface Helper {
    void addNode(Node node);
    String allocateNodeIdKey(UmlClass clazz);
}

@RunWith(MockitoJUnitRunner.class)
public class ClassHandlerTest {
    private static final String TWO_METHODS = "+ toString() : String\n* calc() : int";

    private static final UmlClass CLASS = getClass(null, null, Lists.newArrayList(getField("f1", false), getField("f2", false)));    
    private static final UmlClass ENUM = getClass(Stereotype.ENUMERATION, null, Lists.newArrayList(getField("f1", true), getField("f2", true)));    
    private static final UmlClass IFACE = getClass(Stereotype.INTERFACE, TWO_METHODS, new ArrayList<>());

    @Mock
    private Helper helper;

    private ClassHandler classHandler;
    
    @Before
    public void before() {
        classHandler = ClassHandler.of(helper::addNode, helper::allocateNodeIdKey);
    }
    
    private static UmlClass getClass(Stereotype stereotype, String methods,
            List<UmlField> fieldTypes) {
        return UmlClass.of("foo", Optional.ofNullable(stereotype), 
                Optional.ofNullable(methods), Optional.empty(),
                new ArrayList<>(), MoreObjects.firstNonNull(fieldTypes,
                        new ArrayList<UmlField>()), false);
    }

    private static UmlField getField(String name, boolean enumConstant) {
        return UmlField.of(name, Optional.empty(), String.class,
                Visibility.PUBLIC, enumConstant);
    }

    @Test
    public void testAppend() {
        when(helper.allocateNodeIdKey(CLASS)).thenReturn("X");
        classHandler.append(CLASS);
        verify(helper).allocateNodeIdKey(CLASS);
        verify(helper).addNode(any(Node.class));
    }

    @Test
    public void testCalcNodeHeight() {
        int classHeight = classHandler.calcNodeHeight(CLASS);
        int enumHeight = classHandler.calcNodeHeight(ENUM);
        int ifaceHeight = classHandler.calcNodeHeight(IFACE);
        
        assertEquals(30 + 2*20, classHeight);
        assertEquals(30 + 20 + 2*20, enumHeight);
        assertEquals(30 + 20 + 20 + 2*20, ifaceHeight);
        
    }

    @Test
    public void testCalcWidth() {
        int classWidth = classHandler.calcWidth(CLASS);
        int enumWidth = classHandler.calcWidth(ENUM);
        int ifaceWidth = classHandler.calcWidth(IFACE);
        
        assertEquals("+ f1 : String".length() * 8, classWidth);
        assertEquals("<< enumeration >>".length() * 8, enumWidth);
        assertEquals("+ toString() : String".length() * 8, ifaceWidth);
    }

}
