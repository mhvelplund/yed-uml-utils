package dk.darknight.uml.generator.graphml;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.graphdrawing.graphml.xmlns.Edge;
import org.junit.Test;

import com.yworks.xml.graphml.EdgeLabel;

import dk.darknight.uml.model.UmlClass;
import dk.darknight.uml.model.UmlEdge;
import dk.darknight.uml.model.UmlEdge.EdgeType;

public class EdgeHandlerTest {

    private static final String B = "b";
    private static final String A = "a";
    private List<Edge> result = new ArrayList<>();
    private Consumer<Edge> addEdge = e -> result.add(e);
    private Function<UmlClass, String> getNodeId = c -> c.getName();

    @Test
    public void testAppend() {
        boolean allMatch = Arrays.stream(EdgeType.values()).allMatch(type -> {
            result.clear();
            EdgeHandler.of(addEdge, getNodeId).append(getEdgeWithType(type));
            assertEquals(1, result.size());
            Edge edge = result.get(0);
            assertEquals(A, edge.getSource());
            assertEquals(B, edge.getTarget());
            return true;
        });        
        assertTrue(allMatch);
    }

    private UmlEdge getEdgeWithType(EdgeType type) {
        return UmlEdge.of(getUmlClassStub(A), getUmlClassStub(B), Optional.empty(), Optional.empty(), type);
    }

    private UmlClass getUmlClassStub(String name) {
        return UmlClass.of(name, Optional.empty(), Optional.empty(), Optional.empty(), new ArrayList<>(), new ArrayList<>(), true);
    }

    @Test
    public void testGetLabel() {
        EdgeLabel edgeLabel = EdgeHandler.of(addEdge, getNodeId).getLabel("foo", "bar");
        assertEquals("foo", edgeLabel.getContent().get(0));
        assertEquals("bar", edgeLabel.getModelPosition());
    }

}